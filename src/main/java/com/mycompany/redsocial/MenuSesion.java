package com.mycompany.redsocial;

import java.util.Scanner;
import model.User;
import service.MessageService;
import service.UserService;

public class MenuSesion {
    public static void menuSesion(User user, Scanner sc){
        int option = 0;
        do {
            
            System.out.println("Welcome to Avila's social network, " + user.getFullname() + ".");
            System.out.println("1. Write message.");
            System.out.println("2. Read messages.");
            System.out.println("3. Delete message.");
            System.out.println("4. Edit profile.");
            System.out.println("5. View users.");
            System.out.println("6. Log out.");
            
            option = sc.nextInt();
            switch(option){
                case 1:
                    MessageService.createMessage(user, sc);
                    break;
                case 2:
                    MessageService.listMessage();
                    break;
                case 3:
                    MessageService.deleteMessage(user, sc);
                    break;
                case 4:
                    user = UserService.updateUser(user, sc);
                    break;
                case 5:
                    UserService.listUsers();
                    break;
                default:
                    break;
            }
        } while (option != 6);
    }
}
