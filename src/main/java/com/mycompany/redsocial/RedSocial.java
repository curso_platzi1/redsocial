package com.mycompany.redsocial;
import java.util.Scanner;
import model.User;
import service.UserService;

/**
 *
 * @author Avyla
 */
public class RedSocial {
        
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        mainMenu(sc);
        sc.close();
    }
     
    public static void mainMenu(Scanner sc){
        int option = 0;
        do {            
            System.out.println("Welcome to Avila's social network.");
            System.out.println("1. Sing in.");
            System.out.println("2. Log in.");
            System.out.println("3. Exit");
            
            option = sc.nextInt();
            switch(option){
                case 1:
                    UserService.createUser(sc);
                    break;
                case 2:
                    User user = UserService.login(sc);
                    if(user.getUserId() > 0){
                        MenuSesion.menuSesion(user, sc);
                    }
                    break;
                default:
                    break;
                         
            }
        } while (option != 3);
    }

}
