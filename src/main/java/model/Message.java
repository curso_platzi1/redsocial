package model;

public class Message {
    private int messageId;
    private String message;
    private String date;
    private String fullname;
    
    //Referencia a usuario
    private int userId;
    
    public Message(){}
    
    public Message(String message, int userId){
        this.message = message;
        this.userId = userId;
    }
    
    public Message(int messageId, int userId){
        this.messageId = messageId;
        this.userId = userId;
    }
    
    public Message(int messageId, String message, String date, String fullname) {
        this.messageId = messageId;
        this.message = message;
        this.date = date;
        this.fullname = fullname;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    
    
}
