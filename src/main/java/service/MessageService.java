package service;

import dao.MessageDAO;
import java.util.Scanner;
import model.Message;
import model.User;

public class MessageService {
    public static void createMessage(User user, Scanner sc){
         System.out.println("Write the message you desire, maximum 250 characters.");
         sc.nextLine();
         String newMessage = sc.nextLine();
         Message message = new Message(newMessage,user.getUserId());
         MessageDAO.createMessageDB(message);
   }
    
    public static void listMessage(){
        MessageDAO.listMessageDB();
    }
    
    public static void deleteMessage(User user, Scanner sc){
        System.out.println("Enter the message ID you wish to delete.");
        int idToDelete = sc.nextInt();
        Message message = new Message(idToDelete,user.getUserId());
        MessageDAO.deleteMessageDB(message);
    }
}