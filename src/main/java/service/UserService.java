package service;

import dao.UserDAO;
import java.util.Scanner;
import model.User;
import org.mindrot.jbcrypt.BCrypt;
public class UserService {
    
    
    //Proceso de incriptacion bcrypt utilizando .gensalt()
    public static String hashPassword(String passwordPlain){
        String hashedPassword = BCrypt.hashpw(passwordPlain, BCrypt.gensalt());
        return hashedPassword;
    }
    
    //Verificar si la contraseña ingresada coincide con el hash almacenado
    public static boolean verifyPassword(String passwordPlain, String hashedPassword){
        return BCrypt.checkpw(passwordPlain, hashedPassword);
    }
    
    public static void createUser(Scanner sc){
        System.out.println("Please enter your name.");
        sc.nextLine();
        String fullname = sc.nextLine();
        System.out.println("Please enter your email address.");
        String email = sc.next();
        System.out.println("Please enter your password.");
        String password = sc.next();
        
        String passwordHashed = hashPassword(password);
        
        User user = new User(email, passwordHashed, fullname);
        UserDAO.createUserDB(user);
    }
    
    public static void listUsers(){
        UserDAO.listUsersDB();
    }
    
    public static User updateUser(User user, Scanner sc){
        System.out.println("Please enter your name.");
        sc.nextLine();
        String fullname = sc.nextLine();
        System.out.println("Please enter your email address.");
        String email = sc.next();
        System.out.println("Please enter your password.");
        String password = sc.next();
        
        String passwordHashed = hashPassword(password);
        
        user = new User(user.getUserId(), email, passwordHashed, fullname);
        UserDAO.esditUserDB(user);
        return user;
    }
    
    public static User login(Scanner sc){
        System.out.println("Please enter your email address.");
        String email = sc.next();
        
        System.out.println("Please enter your password.");
        String password = sc.next();
        
        User user = new User(email,password);
        
        User finalUser = UserDAO.loginDB(user);
        return finalUser;
    }
}
