package dao;

import database.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.User;
import service.UserService;

public class UserDAO {
    
    public static void createUserDB(User user){
        DbConnection connection = new DbConnection();
        
        try(Connection cnn = connection.get_Connection()){
            PreparedStatement stmt = null;
            try{
                String query = "INSERT INTO usuarios (correo, clave,nombre_completo) "
                             + "VALUES (?,?,?)";
                stmt = cnn.prepareStatement(query);
                stmt.setString(1, user.getEmail());
                stmt.setString(2, user.getPassword());
                stmt.setString(3, user.getFullname());
                stmt.executeUpdate();
                System.out.println("User created.");
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
    }
    
    public static void listUsersDB(){
        DbConnection connection = new DbConnection();
        try(Connection cnn = connection.get_Connection()){
            PreparedStatement stmt = null;
            ResultSet rs = null;
            try{
                String query = "SELECT id_usuario, correo, nombre_completo "
                             + "FROM usuarios";
                stmt = cnn.prepareStatement(query);
                rs = stmt.executeQuery();
                
                while(rs.next()){
                    System.out.println("ID: " + rs.getInt("id_usuario"));
                    System.out.println("Email: " + rs.getString("correo"));
                    System.out.println("FullName: " + rs.getString("nombre_completo"));
                }
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void deleteUserDB(int id){
        DbConnection connection = new DbConnection();
        try(Connection cnn = connection.get_Connection()){
            PreparedStatement stmt = null;
            try{
                String query = "DELETE "
                             + "FROM usuarios "
                             + "WHERE usuarios.id_usuario = ?";
                stmt = cnn.prepareStatement(query);
                stmt.setInt(1, id);
                stmt.executeUpdate();
                System.out.println("User deleted.");
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static User loginDB(User user){
        DbConnection connection = new DbConnection();
        
        try(Connection cnn = connection.get_Connection()){
            PreparedStatement stmt = cnn.prepareStatement("SELECT * "
                                                        + "FROM usuarios "
                                                        + "WHERE correo = ?");
            stmt.setString(1, user.getEmail());
            
            try(ResultSet rs = stmt.executeQuery()){
                User newUser = new User();
                
                if(rs.next() && UserService.verifyPassword(user.getPassword(), 
                            rs.getString("clave"))){
                    System.out.println("Login successful.");
                    newUser.setUserId(rs.getInt("id_usuario"));
                    newUser.setEmail(rs.getString("correo"));
                    newUser.setFullname(rs.getString("nombre_completo"));
                    return newUser;
                }else{
                    System.out.println("Login failed.");
                }
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }
    
    public static void esditUserDB(User user){
        DbConnection connection = new DbConnection();
        try(Connection cnn = connection.get_Connection()){
            PreparedStatement stmt = null;
            try{
                String query = "UPDATE usuarios "
                             + "SET correo = ?, clave = ?, nombre_completo = ? "
                             + "WHERE id_usuario = ?";
                stmt = cnn.prepareStatement(query);
                stmt.setString(1,user.getEmail());
                stmt.setString(2, user.getPassword());
                stmt.setString(3,user.getFullname());
                stmt.setInt(4,user.getUserId());
                stmt.executeUpdate();
                System.out.println("Successful update.");
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
}
