/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import database.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Message;

public class MessageDAO {
     public static void createMessageDB(Message message){
        DbConnection connection = new DbConnection();
        try(Connection conn = connection.get_Connection()){
            PreparedStatement stmt = null;
            try{
                String query = "INSERT INTO red_social.mensajes (id_usuario, mensaje, fecha) "
                        + "VALUES (?,?,CURRENT_TIMESTAMP)";
                stmt = conn.prepareStatement(query);
                stmt.setInt(1, message.getUserId());
                stmt.setString(2,message.getMessage());
                stmt.executeUpdate();
                System.out.println("Message created");
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void listMessageDB(){
        DbConnection connection = new DbConnection();
        try(Connection cnn = connection.get_Connection()){
            PreparedStatement stmt = null;
            ResultSet rs = null;
            try{
                String query = "SELECT m.id_mensaje, m.mensaje, m.fecha, u.nombre_completo " +
                               "FROM mensajes m " +
                               "LEFT JOIN usuarios u " +
                               "ON m.id_usuario = u.id_usuario";
                stmt = cnn.prepareStatement(query); 
                rs = stmt.executeQuery(query);
                while(rs.next()){
                    System.out.println("ID: " + rs.getInt("id_mensaje"));
                    System.out.println("Message: " + rs.getString("mensaje"));
                    System.out.println("Date: " + rs.getString("fecha"));
                    System.out.println("Author: " + rs.getString("nombre_completo"));
                }
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void udapteMessageDB(Message message){
        DbConnection connection = new DbConnection();
        try(Connection cnn = connection.get_Connection()){
            PreparedStatement stmt = null;
            try{
                String query = "UDAPTE mensajes SET mensaje = ? WHERE mensajes.id_mensaje = ?";
                stmt = cnn.prepareStatement(query);
                stmt.setString(1, message.getMessage());
                stmt.setInt(2, message.getMessageId());
                stmt.executeUpdate();
                System.out.println("Menssge udapted");
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void deleteMessageDB(Message message){
        DbConnection connection = new DbConnection();
        try(Connection cnn = connection.get_Connection()){
            PreparedStatement stmt = null;
            try{
                String query = "DELETE "
                             + "FROM mensajes " 
                             + "WHERE mensajes.id_mensaje = ? AND mensajes.id_usuario = ?";
                stmt = cnn.prepareStatement(query);
                stmt.setInt(1, message.getMessageId());
                stmt.setInt(2, message.getUserId());
                stmt.executeUpdate();
                System.out.println("Message deleted");
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
