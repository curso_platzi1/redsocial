CREATE DATABASE red_social;
USE red_social;
CREATE TABLE mensajes(
	id_mensaje INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  id_usuario INT(10) NOT NULL,
  mensaje VARCHAR(280) NOT NULL,
  fecha TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
)ENGINE = InnoDB DEFAULT CHARSET = utf8;

INSERT INTO mensajes (id_usuario, mensaje, fecha)
VALUES
  (1, 'Hola, ¿cómo estás?', '2023-09-28 10:15:00'),
  (2, '¡Buenos días!', '2023-09-28 10:30:00'),
  (3, '¿Qué planes tienes para hoy?', '2023-09-28 11:05:00'),
  (4, 'Nos vemos a las 3 en el café', '2023-09-28 12:20:00'),
  (5, '¿Puedes pasarme los apuntes de la clase?', '2023-09-28 14:45:00');

CREATE TABLE usuarios(
	id_usuario INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    correo VARCHAR(50) NOT NULL,
    clave VARCHAR(32) NOT NULL,
    nombre_completo VARCHAR(50) NOT NULL
)ENGINE = InnoDB DEFAULT CHARSET = utf8;

ALTER TABLE mensajes
	ADD KEY id_usuario(id_usuario);
    
ALTER TABLE mensajes
	ADD CONSTRAINT fk_id_usuarios FOREIGN KEY (id_usuario) REFERENCES usuarios(id_usuario);

ALTER TABLE usuarios
ADD CONSTRAINT UNIQUE (correo);
